![head](http://www.madarme.co/portada-web.png)
# Título del proyecto:

#### Ejercicio Cuadratica con JavaScript
***
## Índice
1. [Características](#caracter-sticas-)
2. [Contenido del proyecto](#contenido-del-proyecto)
3. [Tecnologías](#tecnologías)
4. [IDE](#ide)
5. [Instalación](#instalación)
6. [Demo](#demo)
7. [Autor(es)](#autores)
8. [Institución Académica](#institución-académica)
9. [Referencias](#referencias)
***

#### Características:

  - Basado en el mockup: [ver](https://www.dropbox.com/s/pi2auhqlianwac5/Actividad%20ecuacion%20segundo%20grado%20-ufps-CSS.pdf?dl=0)
  - Uso de ecuaciones matemáticas en la web mediante CSS: [ver](https://www.periodni.com/es/ecuaciones_matematicas_y_quimicas_en_la_web.html) 
  
***
  #### Contenido del proyecto

| Archivo      | Descripción  |
|--------------|--------------|
| [index.html](https://gitlab.com/JohnnyQuintero/cuadratica-js/-/blob/master/index.html) | Archivo HTML donde contiene la estructuración de la pagina principal. |
| [js/misFunciones.js](https://gitlab.com/JohnnyQuintero/cuadratica-js/-/blob/master/js/misFunciones.js) | Archivo JavaScript con el proceso de calculo de la ecuación, tabla, gráfico y sus funciones adicionales para la impresión de resultados. |
| [css/index.css](https://gitlab.com/JohnnyQuintero/cuadratica-js/-/tree/master/css/index.css) | Archivo CSS que contiene los estilos usados en el HTML. |
| [images/mate.jpg](https://gitlab.com/JohnnyQuintero/cuadratica-js/-/blob/master/images/mate.jpg) | Imagen con el logo representado en la pagina principal. |
  
***
#### Tecnologías

  1. [![HTML5](https://img.shields.io/badge/HTML5-CSS-green)](https://developer.mozilla.org/es/docs/Web/Guide/HTML/HTML5)

   * HTML5 provee básicamente tres características: Estructura, estilo y funcionalidad. HTML5 es considerado el producto de la combinación de HTML, CSS y JavaScript. Estas tecnologías son altamente dependientes y actúan como una sola unidad organizada bajo la especificación de HTML5. HTML está a cargo de la estructura, CSS presenta esa estructura y su contenido en la pantalla y JavaScript hace el resto que es extremadamente significativo. Más alla de la integración, la estructura sigue siendo parte esencial, la misma provee los elementos necesarios para ubicar contenido estático y dinámico, y es también una plataforma básica para aplicaciones.(Gauchat, 2012)
    Usted puede ver el siguiente marco conceptual y aprender sobre HTML5: [Guia completa de HTML5](https://www.w3schools.com/html/default.asp)

   * CSS define el aspecto gráfico de los elementos HTML. Éstos pueden ser definidos en una hoja de estilos externa o internamente en el propio documento HTML. Cuando se define en un archivo externa, los estilos pueden ser compartidos por muchas páginas, lo que le permite cambiar instantaneamente la apariencia visual de todas las páginas modificando solo el archivo en el que se definen los estilos.
El uso de CSS da consistencia, ahorra tiempo y facilita en gran medida la escritura de las páginas web, haciendolas más flexibles y ligeras, controlando su aspecto gráfico con mayor precisión y mayor facilidad para la correción de errores.
CSS es el formato recomendado para las páginas escritas en formato HTML en base a los estándares de "Cascading Style Sheets", publicado por el World Wide Web Consortium. (Durango, 2015)
      Usted puede ver el siguiente marco conceptual y aprender sobre CSS: [Guia completa de CSS](https://www.w3schools.com/css/default.asp)

  2. [![JavaScript](https://img.shields.io/badge/JAVASCRIPT-orange)](https://developer.mozilla.org/es/docs/Web/JavaScript)
  
   * JavaScript es un lenguaje de programación que se utiliza principalmente para crear páginas web dinámicas. Es un lenguaje de programación interpretado, por lo que no es necesario compilar los programas para ejecutarlos, es decir, los programas escritos con JavaScript se pueden probar directamente en cualquier navegador sin necesidad de procesos intermedios. (Pérez, 2019)

     Usted puede ver el siguiente marco conceptual y aprender sobre JavaScript: [Guia completa de JS](https://www.w3schools.com/js/DEFAULT.asp)

  3. [![BOOTSTRAP](https://img.shields.io/badge/BOOTSTRAP-blueviolet)](https://getbootstrap.com)    
 
   * Bootstrap es una biblioteca multiplataforma o conjunto de herramientas de código abierto para diseño de sitios y aplicaciones web. Bootstrap  permite crear interfaces web con CSS y JavaScript, cuya particularidad es la de adaptar la interfaz del sitio web al tamaño del dispositivo en que se visualice

     Usted puede ver el siguiente marco conceptual sobre Bootstrap: [Biblioteca multiplataforma de Bootstrap](https://getbootstrap.com/)

Para la realización de graficas se uso la API de Google Charts:

  - [![Google Charts](https://img.shields.io/badge/GOOGLE_CHARTS-blue)](https://developers.google.com/chart)

  Usted puede ver el siguiente marco conceptual sobre Google Charts:

   - [API de Google Charts](https://developers.google.com/chart)
   - [Vídeo explicativo funciones de Google Charts](https://www.youtube.com/watch?v=QRN91T8rqW4&feature=emb_logo)
  
  ***
#### IDE

El proyecto se desarrolla usando Sublime Text 3, es un editor de texto para código en diferentes lenguajes, se destaca su versatilidad para el manejo de marcas y autocompletado. [(Kinder, 2013)](#kinder-k-2013-sublime-text-one-editor-to-rule-them-all-linux-journal-2013232-2).

Ventajas de Sublime Text:
 - Muy liviano, fácil de instalar y tiene una versión portable.
 - Resalta todo tipo de lenguaje con colores para visualmente detectar fallos a simple vista.
 - Para aprender es una buena opción, porque te ayuda pero no te lo da todo hecho.
 - Funciona en Windows, Mac y Linux.
   

***
### Instalación

 1. Local
  - Descargar el repositorio ubicado en - [descargar](http://gitlab.com/JohnnyQuintero/cuadratica-js)
  - Invocar oágina index.html desde el navegador predeterminado.
2. Gitlab
  - Realizar un "fork" del repositorio ubicado en - [ir](http://gitlab.com/JohnnyQuintero/cuadratica-js)
  - Crear templates de html.
  - Realizar un "commit" para guardar los cambios efectuados.
  - Dar clic a "create merge request" y a continuación dar clic en "merge".
  - Dirigirse a la sección "setting pages" para obtener el enlace del hosting.


***
### Demo

Para ver el demo de la aplicación puede dirigirse a: [Cuadratica](https://johnnyquintero.gitlab.io/cuadratica-js/).

***
### Autor(es)
Proyecto desarrollado por [Johnny Quintero](<johnnyalexanderqr@ufps.edu.co>) y [Matilde Arevalo](<matildealexandraal@ufps.edu.co>).
***
### Institución Académica   
Proyecto desarrollado en la Materia programación web del  [Programa de Ingeniería de Sistemas] de la [Universidad Francisco de Paula Santander]

   [Programa de Ingeniería de Sistemas]:<https://ingsistemas.cloud.ufps.edu.co/>
   [Universidad Francisco de Paula Santander]:<https://ww2.ufps.edu.co/>
   
   ***
### Referencias
   
- Durango, A. (2015). Diseño Web con CSS: 2ª Edición. IT Campus Academy
- Gauchat, J. D. (2012). El gran libro de HTML5, CSS3 y Javascript. Marcombo.
- Kinder, K. (2013). Sublime text: one editor to rule them all?. Linux Journal, 2013(232), 2.
- Pérez, J. E. (2019). introduccion a JavaScript.

