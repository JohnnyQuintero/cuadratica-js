var numA = 0;
var numB = 0;
var numC = 0;
var intervalo_inicio = 0;
var intervalo_final = 0;

function leer_datoABC() {
    let datos = document.getElementsByName("entrada");
    if (validarDatos(datos)) {
        numA = parseFloat(datos[0].value);
        numB = parseFloat(datos[1].value);
        numC = parseFloat(datos[2].value);
    } else {
        alert("No puede insertar valores invalidos, digite otros valores númericos");
        return false;
    }
    return true;
}

function leer_datoIntervalos() {
    let datosIntervalo = document.getElementsByName("intervalos");
    if (validarDatos(datosIntervalo)) {
        intervalo_inicio = parseInt(datosIntervalo[0].value);
        intervalo_final = parseInt(datosIntervalo[1].value);
    } else {
        alert("No puede insertar valores invalidos, digite otros valores númericos");
        return false;
    }
    return true;
}

function validarDatos(datos) {
    for (var i = 0; i < datos.length; i++) {
        if (isNaN(datos[i].value) || datos[i].value == "")
            return false;
    }
    return true;
}

function calcular() {
    let ans1 = 0,
        ans2 = 0;
    let rta = "";

    if (leer_datoABC()) {

        let discriminante = (numB * numB) - (4 * (numA) * (numC));

        if (numA == 0) {
            rta = "<div  class='col text-center' id='err'><label class='text-white'>" +
                "El coeficiente a no puede ser igual que cero, digite otro valor.</label></div>";
        } else if (discriminante < 0) {
            rta = "<div  class='col text-center' id='err'><label class='text-white'>" +
                "La ecuación no tiene soluciones reales, digite otro valor.</label></div>";
        } else {
            let raiz = Math.sqrt(discriminante);
            ans1 = ((-numB) + raiz) / (2 * numA);
            ans2 = ((-numB) - raiz) / (2 * numA);

            mostrarEcuacion();

            rta = "<div class='fra' >" +
                "<i>x =<div class='fraction'>-<span class='fup'><i style='color:red'>(" + numB + ")</i>±<span class='radical'>&radic;</span>" +
                "<span class='radicand'><i style='color:red'>(" + numB + ")<sup>2</sup></i> - 4 <i style='color:lime'>(" + numA + ")</i> <i style='color:blue'>(" + numC + ")</i></i></span></span>" +
                "<span class='bar'>/</span>" +
                "<span class='fdn'><i>2</i><i style='color:lime'>(" + numA + ")</i></span></div>" +
                "</div> <div class = 'row' id = 'ltt2' style = 'margin-left: 130px;'>" +
                "<div class='col-5'>x<sub>1</sub>= <b>" + ans1.toFixed(2) + "</b></div>" +
                "<div class = 'col-7'>x <sub>2</sub>= <b>" + ans2.toFixed(2) + "</b></div> </div>";
        }
        document.getElementById("visualizar").innerHTML = rta;
    }
}


function intervalos() {
    let valores;
    if (leer_datoIntervalos()) {
        if (intervalo_inicio > intervalo_final) {
            alert("Los rangos no coinciden, favor digite otros valores");
        } else if (intervalo_final == intervalo_inicio) {
            alert("No pueden tener el mismo valor para el rango, favor digite otro valores");
        } else {
            valores = new Array(intervalo_final - intervalo_inicio + 1);

            mostrarEcuacion();

            for (var i = intervalo_inicio, j = 0; j < valores.length; i++, j++) {
                valores[j] = calulo_intervalos(i);
            }
        }
    }
    return valores;
}

function calulo_intervalos(i) {
    // body...
    let ans = 0;
    ans = ((numA * (i * i)) + (numB * i) + numC);
    return ans;
}


function mostrarEcuacion() {
    let rta = "";
    rta = "<div id='ltt' style=' margin-top: 50px; margin-left: 50px;'>" +
        "Cálculo para: <b style='color: lime'>" + numA + "</b><b><i style='color: lime' >x</i></b>" +
        "<b><sup style='color: lime'>" + 2 + "</sup></b>";
    if (numB > 0) {
        rta += "<b style='color: red'> +" + numB + "<i style='color: red' >x</i></b>";
    } else {
        rta += "<b style='color: red'> " + numB + "<i style='color: red' >x</i></b>";
    }
    if (numC > 0) {
        rta += "<b style='color: blue'> +" + numC + " </b> <b>= 0</b> </div>";
    } else {
        rta += "<b style='color: blue'> " + numC + " </b> <b>= 0</b> </div>";
    }

    document.getElementById("ecuacion").innerHTML = rta;
    return rta;
}

function mostrarGrafico(select) {
    var data = new google.visualization.DataTable();

    data.addColumn('number', 'x');
    data.addColumn('number', 'y');

    let resultado = new Array();
    resultado = intervalos();

    data.addRows(resultado.length);

    for (var i = 0; i < resultado.length; i++) {
        data.setCell(i, 0, (intervalo_inicio++));
        data.setCell(i, 1, resultado[i]);
    }

    if (select) {
        var titulo = {
            'title': 'Gráfico parala función ' + numA + 'x2 +' + numB + 'x ' + numC + ' = 0',
            'width': 900,
            'height': 500,
        };
        var chart = new google.visualization.LineChart(document.getElementById('visualizar'));
        chart.draw(data, google.charts.Line.convertOptions(titulo));
    } else {
        var table = new google.visualization.Table(document.getElementById('visualizar'));
        table.draw(data, { showRowNumber: false, width: "10%", height: '40%' });
    }
}